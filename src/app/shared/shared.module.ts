import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PasswordModule} from 'primeng/password';
import { PasswordComponent } from './components/password/password.component';
import {DividerModule} from 'primeng/divider';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    PasswordComponent,
  ],
  imports: [
    CommonModule,
    PasswordModule,
    DividerModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  exports: [
    PasswordModule,
    DividerModule,
    PasswordComponent
  ]
})
export class SharedModule { }
