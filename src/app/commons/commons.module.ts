import { NgModule } from '@angular/core';
import { MainContentComponent } from './content/main-content.component';
import { HeaderComponent } from './header/header.component';
import { TopNavBarComponent } from './header/top-nav-bar/top-nav-bar.component';
import { NavBarComponent } from './header/nav-bar/nav-bar.component';
import { SidebarComponent } from './l-side-bar/sidebar.component';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MainContentComponent,
    TopNavBarComponent,
    NavBarComponent,
    SidebarComponent,
    FooterComponent,
  ],
  imports: [RouterModule],
  exports: [MainContentComponent, HeaderComponent, SidebarComponent],
  providers: [],
})
export class CommonsModule {}
