# Estructura

~~~

|-- app
   |-- comunes
      |-- [+] footer
      |-- [+] header
      |-- [+] sidebar
      |-- [+] rightsidebar
   |-- web
      |-- dashboard
         |-- [+] componentes
         |-- [+] pages
         |-- [+] interfaces
         |-- [+] services
         |-- dashboard-routing.module.ts
         |-- dashboard.module.ts
      |-- pacientes
         |-- [+] componentes
         |-- [+] paginas
             |-- [+] paciente-informacion
             |-- [+] paciente-citas
         |-- [+] interfaces
         |-- [+] services
         |-- paciente-routing.module.ts
         |-- paciente.module.ts
   |-- core 
      |-- [+] autenticación
      |-- [+] guards
      |-- [+] http
      |-- [+] interceptors
      |-- [+] mocks
      |-- [+] services
      |-- core.module.ts
      |-- ensureModuleLoadedOnceGuard.ts
      |-- logger.service.ts
   |-- shared
      |-- [+] componentes
      |-- [+] directivas
      |-- [+] pipes
      |-- [+] modelos
   |-- [+] configs

|-- assets
   |-- css
      |-- [+] partials
      |-- _base.css
      |-- styles.css

~~~

## Archivos internos

Los archivos internos deben de generarse con el CLI, salvo aquellos que estan generados que con el code-snippet de angular se puede generar el código interno, así los archivos que estan creados pueden guiarnos.

# EmrWeb

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.4.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
