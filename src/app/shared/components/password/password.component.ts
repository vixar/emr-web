import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styles: [`
      :host ::ng-deep .p-password input {
          width: 15rem
      }
  `]
})
export class PasswordComponent implements OnInit {

  value1 = '';

  value2 = '';

  value3 = '';

  value4 = '';

  constructor() { }

  ngOnInit(): void {
  }

}

