
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/content/dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [DashboardComponent],
  imports: [SharedModule],
  exports: [],
  providers: [],
})
export class WebModule { }
